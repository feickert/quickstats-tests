default: image

all: image

# global build variables
BASE_IMAGE=atlasamglab/stats-base:root6.24.06
QUICKSTATS_VERSION=0.5.6.3

image:
	docker pull $(BASE_IMAGE)
	docker build . \
	-f Dockerfile \
	--build-arg BUILDER_IMAGE=$(BASE_IMAGE) \
	--tag atlasamglab/quickstats:$(QUICKSTATS_VERSION) \
	--tag atlasamglab/quickstats:$(QUICKSTATS_VERSION)-root6.24.06 \
	--tag atlasamglab/quickstats:latest
