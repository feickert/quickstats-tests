#!/bin/bash

RUN_DIR="${1:-example}"

# Cleanup from previous runs
if [ -f "benchmark.txt" ]; then
    mv benchmark.txt{,.bak}
fi
if [ -f "limits.json" ]; then
    mv limits.json{,.bak}
fi
if [ -f "limits_summary.json" ]; then
    mv limits_summary.json{,.bak}
fi

# wrap in () to captrue output of time
(time quickstats cls_limit \
    -d asimovData \
    -i "${RUN_DIR}"/config/FitConfig_combined_ggf_and_vbf_meas_model.root \
    --fix Lumi=1 \
    -v DEBUG \
    --print_level 2) \
    &> benchmark.txt
