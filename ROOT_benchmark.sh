#!/bin/bash

RUN_DIR="${1:-example}"

# Cleanup from previous runs
if [ -f "ROOT_benchmark.txt" ]; then
    mv ROOT_benchmark.txt{,.bak}
fi

# wrap in () to captrue output of time
(time python ROOT_benchmark.py \
    "${RUN_DIR}"/config/FitConfig_combined_ggf_and_vbf_meas_model.root) \
    &> ROOT_benchmark.txt
