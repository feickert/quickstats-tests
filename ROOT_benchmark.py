import array
import sys
import timeit

import numpy as np
import ROOT

if __name__ == "__main__":
    # sys.argv[1] should be the ROOT file with the serialized workspace
    input_file = ROOT.TFile.Open(sys.argv[1])
    workspace = input_file.Get("combined")
    pdf = workspace.pdf("simPdf")
    data = workspace.data("obsData")

    print(workspace)

    nll = pdf.createNLL(data)

    iter = workspace.components().fwdIterator()
    while True:
        n = iter.next()
        try:
            if n.IsA() == ROOT.RooRealSumPdf.Class():
                print("set!")
                n.setAttribute("BinnedLikelihood")
        except ReferenceError:
            break

    minim = ROOT.RooMinimizer(nll)
    minim.setMinimizerType("Minuit2")
    fcn = ROOT.RooMinimizerFcn(nll, minim)
    fitter = minim.fitter()
    fitter.FitFCN(fcn)
