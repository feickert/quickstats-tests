#!/bin/bash

docker pull atlasamglab/quickstats:0.5.6.3
docker run \
    --privileged \
    --rm \
    -ti \
    -v $PWD:/home/data \
    -w /home/data \
    docker.io/atlasamglab/quickstats:0.5.6.3
