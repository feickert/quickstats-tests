ARG BUILDER_IMAGE=atlasamglab/root-base:root6.24.06
FROM ${BUILDER_IMAGE} as builder

ARG QUICKSTATS_VERSION=0.5.6.3

SHELL [ "/bin/bash", "-c" ]

RUN python -m pip --no-cache-dir install --upgrade pip setuptools wheel && \
    python -m pip --no-cache-dir install "quickstats==${QUICKSTATS_VERSION}" && \
    quickstats compile && \
    python -m pip list && \
    quickstats --help
